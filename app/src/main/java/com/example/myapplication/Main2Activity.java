package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Main2Activity extends AppCompatActivity {

    EditText username, password, card, email;

    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("http://intelligent-system.online")
            .addConverterFactory(GsonConverterFactory.create())
            .build();
    Boolean boobs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        username = findViewById(R.id.editText3);
        password = findViewById(R.id.editText4);
        card = findViewById(R.id.editText5);
        email = findViewById(R.id.editText6);
    }

    public void onRegClicked(View v) {
        if (!TextUtils.isEmpty(username.getText()) && !TextUtils.isEmpty(password.getText()) && !TextUtils.isEmpty(card.getText()) && !TextUtils.isEmpty(email.getText())) {
            if (String.valueOf(email.getText()).indexOf("@") != 0) {
                SomeAPI api = retrofit.create(SomeAPI.class);
                api.basicReg(new User(String.valueOf(username.getText()), String.valueOf(email.getText()), String.valueOf(password.getText()), String.valueOf(card.getText()))).enqueue(new Callback<User>() {
                    @Override
                    public void onResponse(Call<User> call, Response<User> response) {
                        if (response.isSuccessful()) {
                            Toast.makeText(Main2Activity.this, "Succesfull!", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(Main2Activity.this, MainActivity.class);
                            startActivity(intent);
                        } else {
                            Toast.makeText(Main2Activity.this, response.message(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<User> call, Throwable t) {
                        Toast.makeText(Main2Activity.this, "Error404", Toast.LENGTH_SHORT).show();
                    }
                });
            } else {
                Toast.makeText(this, "Нет знака @!", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, "Поле(я) пустое(ые)", Toast.LENGTH_SHORT).show();
        }
    }

    public void onCancelClicked(View v) {
        Intent intent = new Intent(Main2Activity.this, MainActivity.class);
        startActivity(intent);
    }
}
