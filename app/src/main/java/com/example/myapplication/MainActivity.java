package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("http://intelligent-system.online")
            .addConverterFactory(GsonConverterFactory.create())
            .build();

    EditText username, password;

    SharedPreferences sh;
    SharedPreferences.Editor ed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        username = findViewById(R.id.editText);
        password = findViewById(R.id.editText2);
        sh = getSharedPreferences("Bruh", 0);
        ed = sh.edit();
    }

    public void onClicked(View v) {
        if (!TextUtils.isEmpty(username.getText()) && !TextUtils.isEmpty(password.getText())) {
            SomeAPI api = retrofit.create(SomeAPI.class);
            api.basicLog(new User(String.valueOf(username.getText()), String.valueOf(password.getText()))).enqueue(new Callback<User>() {
                @Override
                public void onResponse(Call<User> call, Response<User> response) {
                    if (response.isSuccessful()) {
                        ed.putString("token", response.body().getToken());
                        ed.commit();
                        Intent intent = new Intent(MainActivity.this, Main3Activity.class);
                        startActivity(intent);
                    } else {
                        Toast.makeText(MainActivity.this, response.message(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<User> call, Throwable t) {
                    Toast.makeText(MainActivity.this, "Error404", Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            //Toast.makeText(this, "Поле(я) пустое(ые)", Toast.LENGTH_SHORT).show();
        }
    }

    public void onRegClicked(View v) {
        Intent intent = new Intent(MainActivity.this, Main2Activity.class);
        startActivity(intent);
    }
}
