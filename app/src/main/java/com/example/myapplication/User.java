package com.example.myapplication;

import com.google.gson.annotations.SerializedName;

public class User {
    @SerializedName("username")
    private String username;
    @SerializedName("password")
    private String password;
    @SerializedName("token")
    private String token;
    @SerializedName("email")
    private String email;
    @SerializedName("id")
    private String id;
    @SerializedName("card")
    private String card;


    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public User(String username, String email, String password, String card) {
        this.username = username;
        this.password = password;
        this.email = email;
        this.card = card;
    }

    public String getToken() {
        return token;
    }
}
