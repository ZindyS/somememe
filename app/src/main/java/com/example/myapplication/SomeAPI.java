package com.example.myapplication;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface SomeAPI {
    @POST("/api/signin")
    Call<User> basicLog(@Body User u);
    @POST("/api/signup")
    Call<User> basicReg(@Body User u);
}
